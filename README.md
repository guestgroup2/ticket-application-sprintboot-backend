# ticket-application-sprintboot-backend

## Scenario

For a journey, a passenger can carry up to 5 items of luggage. Clerks need to register the weight of
each one and each piece of baggage is assigned a code. After entering all items, a fee for the
baggage is calculated as follows:
1. 1 small item up to 7kg is free
2. 1 big item up to 25kg is free
3. Fee for an item weighing 0-7kg is 10 euro
4. Fee for an item weighing 7-25kg is 25 euro
5. Fee for overweight item is 5 euro per kg.
It is possible to add overweight fee for free items if it is advantageous for the passenger, i.e. a pair of
9kg and 15kg items would result in a fee of 10 euro (2kg overweight on top of the 7kg free item)
Sample application should offer 5 fixed tickets (5 passengers to check in), we don’t need any edit
functionality for these.
After selecting a ticket, a baggage check screen should be presented where the clerk can type in
baggage weights.
After confirming baggage weights, the application either automatically chooses the lowest possible
fee, or gives options to the clerk to categorize type of luggage (free, free + overweight, light, heavy,
…) and then computes the fee. Conditions for each type need to be verified.
Once the fee is determined, the application displays an overview of luggage IDs, their weight, and
respective fee, as well as total.

Baggage Category:
1. Label: Free, Value: FREE, Description: It represents the free of charge of baggage.
2. Label: Overweight, Value: FREE_OVER, Description: It represents the free part of 0-7kg or up 25kg free + overweight part.
3. Label: Light, Value: LIGHT, Description: It represents the baggage that weight is between 0 and 7kg.
4. Label: Heavy, Value: HEAVY, Description: It represents the baggage that weight is between 7kg and 25kg.
5. Label: Extreme Heavy, Value: EXT, Description: It represents the baggage that weight is larger than 25KG.

## System Workflow and Operation Diagram

Reference Link: [Workflow Diagram](https://gitlab.com/guestgroup2/ticket-application-sprintboot-backend/-/blob/main/ticket_system_workflow_architecture.drawio.png)

## System Architecture & Structure

1.	It is a Springboot 3 application. (Springboot 3, Spring Framework 5 and Spring Webflux Web Application, Java SDK 20)
2.	Maven Project (dependency on porn.xml)
3.	Design Pattern: Builder
4.	Communication method between front and back ends: RESTful Call
5.	Test: located at src/test/java/com/eddie/ticket/server/ticketApi/
    1.	JUnit 5 is used.
    2.	ticketApi.java class
        1.	method 1: test the auto generation of tickets.
        2.	Method 2: test the minimum amount of ticket and amount per luggage by weight.
        3.	Method 3: test the amount of ticket and amount per luggage by category.
6.	Structure: 
    1.	com.eddie.ticket.server.entity:
        1.	Contains ticket, item, passenger, staff, message.
            1.	Ticket: 
                1.	contains the method and attribute of a ticket and it contains a set of item, a passenger and staff objects. 
                2.	Support cloneable object.
                3.	Support builder design pattern to build a ticket.
            2.	Item: 
                1.	contains the method and attribute of a luggage. 
                2.	Support cloneable object.
                3.	Support builder design pattern to build a luggage.
            3.	Passenger:
                1.	Describe a passenger of a ticket.
            4.	Staff:
                1.	Describe a clerk that handles a ticket.
            5.	Message:
                1.	Describe whether the amount of ticket calculation by category success or not. (contains success, message and data attributes)
    2.	com.eddie.ticket.server.Dto:
        1.	Contains ticketDto.
            1.	ticketDto:
                1.	The fields necessary to display on the front-end (View)
                2.	It is used by the data mapper methods to convert between ticket and ticketDto (View)
    3.	com.eddie.ticket.server.service:
        1.	Contains impl package and ticketService:
            1.	Impl package contains ticketServiceImpl
                1.	ticketServiceImpl:
                2.	It implements the ticketService interface and contains method enough to calculate the minimum amount of ticket by weight, the amount of ticket by category, the ticket generation and the mappers to convert between ticket object and ticketDto object.
            2.	ticketService:
                1.	It is an interface class that handles ticket operations and it is used by the ticketController (REST api).
    2.	com.eddie.ticket.server.controller:
        1.	Contains TicketController class:
            1.	It is an API of RESTful and is called by the front-end to generate tickets, calculate minimum amount of ticket and calculate the ticket amount by category.

## Installation and Run Application

1.	Commands (under the project folder): mvn clean compile test
2.	Or open it on IntelliJ. 
    1.	On the Maven tab/menu:
        1.	Click clean.
        2.	Click compile
        3.	Click test (if you want to test the application)
        4.	Click package (if you want to generate .jar file. Command: jar abc.jar under the project folder/target/)
        5.	Configuration the run operation using the TicketApplication.java to run the application on IntelliJ.
        6. Steps refer to the links below:
            1. [Compile](https://gitlab.com/guestgroup2/ticket-application-sprintboot-backend/-/blob/main/backend_compile_steps.png)
            2. [Configuration Run Step 1](https://gitlab.com/guestgroup2/ticket-application-sprintboot-backend/-/blob/main/back-end-configuration-run1.png)
            3. [Configuration Run Step 2](https://gitlab.com/guestgroup2/ticket-application-sprintboot-backend/-/blob/main/back-end-configuration-run2.png)
            4. [Configuration Run Step 3](https://gitlab.com/guestgroup2/ticket-application-sprintboot-backend/-/blob/main/back-end-configuration-run3.png)
3.	After the application is started open browser and enter: http://localhost:8087/generate to test.

## RESTful API Instruction

Reference Document: [RESTful API Document](https://gitlab.com/guestgroup2/ticket-application-sprintboot-backend/-/blob/main/RESTful_API_Document.docx)

| RESTful URL | Input Params | Description | Output |
| ------ | ------ | ------ | ------ |
|    GET: /generate    |  N/A      | 1.	URL to generate tickets & luggage. <br><br>2.	(By default, 1 ticket is hardcoded on the method. Future, it is allowed to alter the number of times of for-loop to generate 5 tickets or add the number of ticket input param to control the number of tickets to be generated.)|1.	JSON String: Array <br><br>2.	Format:<br>a.	E.g. [{id: 0, ticketId: 0, ticketNo: “T123”, passenger: “Peter”, original: “LHR”, destination: “SYD”, clerk: “Mary”, itemId: 0, itemNo: “L1234”, weight: 0.0, charge: 0.0, amount: 0.0}]<br><br>3.	Attribute description:<br>a.	Id: Integer (It is used for the grid component on front-end only.)<br>b.	ticketId: Integer (the unique ID of a ticket)<br>c.	ticketNo: String (the code of a ticket/ ticket identifier)<br>d.	passenger: String (the name of a passenger)<br>e.	original: String (IATA port code of the starting port)<br>f.	destination: String (IATA port code of the destination port)<br>g.	 clerk: String (the name of the clerk)<br>h.	itemId: Integer (the unique number of luggage)<br>i.	itemNo: String (the code of a luggage/the identifier of luggage)<br>j.	weight: double (the weight of luggage(item))<br>k.	charge: double (the individual charge of luggage(item))<br>l.	amount: double (the total amount of a ticket)|
|POST: /minWeight|1.	JSON String: Array <br><br>2.	Format:<br>a.	E.g. [{id: 0, ticketId: 0, ticketNo: “T123”, passenger: “Peter”, original: “LHR”, destination: “SYD”, clerk: “Mary”, itemId: 0, itemNo: “L1234”, weight: 0.0, charge: 0.0, amount: 0.0}]<br><br>3.	Attribute description:<br>a.	Id: Integer (It is used for the grid component on front-end only.)<br>b.	ticketId: Integer (the unique ID of a ticket)<br>c.	ticketNo: String (the code of a ticket/ ticket identifier)<br>d.	passenger: String (the name of a passenger)<br>e.	original: String (IATA port code of the starting port)<br>f.	destination: String (IATA port code of the destination port)<br>g.	 clerk: String (the name of the clerk)<br>h.	itemId: Integer (the unique number of luggage)<br>i.	itemNo: String (the code of a luggage/the identifier of luggage)<br>j.	weight: double (the weight of luggage(item))<br>k.	charge: double (the individual charge of luggage(item))<br>l.	amount: double (the total amount of a ticket)|To calculate ticket minimum amount by weight.|The JSON string same as the input param. Only the charge attribute per luggage and the amount attribute of ticket is filled.|
|POST: /minCategory|1.	JSON String: Array <br><br>2.	Format:<br>a.	E.g. [{id: 0, ticketId: 0, ticketNo: “T123”, passenger: “Peter”, original: “LHR”, destination: “SYD”, clerk: “Mary”, itemId: 0, itemNo: “L1234”, weight: 0.0, charge: 0.0, amount: 0.0}]<br><br>3.	Attribute description:<br>a.	Id: Integer (It is used for the grid component on front-end only.)<br>b.	ticketId: Integer (the unique ID of a ticket)<br>c.	ticketNo: String (the code of a ticket/ ticket identifier)<br>d.	passenger: String (the name of a passenger)<br>e.	original: String (IATA port code of the starting port)<br>f.	destination: String (IATA port code of the destination port)<br>g.	 clerk: String (the name of the clerk)<br>h.	itemId: Integer (the unique number of luggage)<br>i.	itemNo: String (the code of a luggage/the identifier of luggage)<br>j.	weight: double (the weight of luggage(item))<br>k.	charge: double (the individual charge of luggage(item))<br>l.	amount: double (the total amount of a ticket)|To calculate the ticket amount by category per luggage. (validate against the luggage rules)|1.	JSON String<br><br>2.	Format:<br>a.	E.g. {success: true, message:””, data: [{id: 0, ticketId: 0, ticketNo: “T123”, passenger: “Peter”, original: “LHR”, destination: “SYD”, clerk: “Mary”, itemId: 0, itemNo: “L1234”, weight: 8.0, charge: 0.0, amount: 0.0}]}<br><br>3.	Attribute description:<br>a.	success: Boolean (describe the rule validation success or not)<br>b.	message: String/Text (describe the error message if breaking the rule. It is empty if success is true)<br>c.	data: array of object (it is the data array containing the ticket and luggage data. The format is same as the input param)


