package com.eddie.ticket.server.api;

import com.eddie.ticket.server.entity.Message;
import com.eddie.ticket.server.entity.Ticket;
import com.eddie.ticket.server.dto.TicketDto;
import com.eddie.ticket.server.service.TicketService;
import com.eddie.ticket.server.service.impl.TicketServiceImpl;
import com.fasterxml.jackson.core.JsonGenerationException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.io.IOException;
import java.util.List;

public class ticketApi {

    TicketService ticketService = new TicketServiceImpl();

    public String getTickets() {
        List<Ticket> waybills = ticketService.getTickets();

        System.out.println(waybills.toString());
        waybills.forEach(ticket -> {
            ticketService.getMiniPayment(ticket);
            ticket.getLuggageList().forEach( item -> {
                System.out.println(ticket.getTicketNo() + " - " + item.getCode() + ": " + item.getWeight());
            });
            System.out.println(ticket.getTicketNo() + " total: " + ticket.getAmount());
        });
        System.out.println("Hello ");
        return "success";
    }

    public String generateTickets() {
        List<Ticket> waybills = ticketService.getTickets();
        List<TicketDto> dto = ticketService.ticketToTicketDto(waybills);

        System.out.println(waybills.toString());
        waybills.forEach(ticket -> {
            ticket.getLuggageList().forEach( item -> {
                System.out.println(ticket.getTicketNo() + " - " + item.getCode() + ": " + item.getWeight());
            });
            System.out.println(ticket.getTicketNo() + " total: " + ticket.getAmount());
        });
        System.out.println("Hello ");
        try {
            ObjectMapper mapper = new ObjectMapper();
            String jsonInString = mapper.writerWithDefaultPrettyPrinter().writeValueAsString(dto);
            System.out.println(jsonInString);
            return jsonInString;
        } catch (JsonGenerationException e) {
            e.printStackTrace();
        } catch (JsonMappingException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

        return "success";
    }

    public List<TicketDto> minWeight(List<TicketDto> dto) {
        List<Ticket> waybills = ticketService.ticketDtoToTicket(dto);
        waybills.forEach(ticket -> {
            ticketService.getMiniPayment(ticket);
            ticket.getLuggageList().forEach( item -> {
                System.out.println(ticket.getTicketNo() + " - " + item.getCode() + ": " + item.getWeight());
            });
            System.out.println(ticket.getTicketNo() + " total: " + ticket.getAmount());
        });
        List<TicketDto> alteredDto = ticketService.ticketToTicketDto(waybills);
        /*try {
            ObjectMapper mapper = new ObjectMapper();
            String jsonInString = mapper.writerWithDefaultPrettyPrinter().writeValueAsString(alteredDto);
            System.out.println(jsonInString);
            return jsonInString;
        } catch (JsonGenerationException e) {
            e.printStackTrace();
        } catch (JsonMappingException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return "success";*/
        return alteredDto;
    }

    public Message minCategory(List<TicketDto> dto) {
        try{
            ObjectMapper mapper = new ObjectMapper();
            Message msg = new Message();
            List<Ticket> waybills = ticketService.ticketDtoToTicket(dto);
            for (int p = 0; p < waybills.size(); p++) {
                msg = ticketService.getCategoryPayment(waybills.get(p));
                System.out.println(msg.isSuccess());
                if (!msg.isSuccess()) {
                    msg.setData(mapper.writerWithDefaultPrettyPrinter().writeValueAsString(dto));
                    //return mapper.writerWithDefaultPrettyPrinter().writeValueAsString(msg);
                    return msg;
                }
            }
            List<TicketDto> alteredDto = ticketService.ticketToTicketDto(waybills);
            String jsonInString = mapper.writerWithDefaultPrettyPrinter().writeValueAsString(alteredDto);
            msg.setData(jsonInString);
            return msg;
            //return mapper.writerWithDefaultPrettyPrinter().writeValueAsString(msg);
        } catch (JsonGenerationException e) {
            e.printStackTrace();
        } catch (JsonMappingException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return new Message();
        //return "success";
    }
}
