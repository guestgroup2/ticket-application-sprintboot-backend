package com.eddie.ticket.server.controller;

import com.eddie.ticket.server.entity.Message;
import com.eddie.ticket.server.entity.Ticket;
import com.eddie.ticket.server.dto.TicketDto;
import com.eddie.ticket.server.mapper.TicketMapper;
import com.eddie.ticket.server.service.ITicketStrategy;
import com.eddie.ticket.server.service.TicketService;
import com.eddie.ticket.server.service.impl.CategoryStrategy;
import com.eddie.ticket.server.service.impl.MinimumWeightStrategy;
import com.fasterxml.jackson.core.JsonGenerationException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.io.IOException;
import java.util.List;

@CrossOrigin
@Slf4j
@RestController
public class TicketController {

    @Autowired
    TicketService ticketService;

    @GetMapping("/generate")
    public String generateTickets() {
        List<Ticket> waybills = ticketService.getTickets();
        List<TicketDto> dto = TicketMapper.TicketToTicketDto(waybills);

        waybills.forEach(ticket -> {
            ticket.getLuggageList().forEach( item -> {
                System.out.println(ticket.getTicketNo() + " - " + item.getCode() + ": " + item.getWeight());
            });
            System.out.println(ticket.getTicketNo() + " total: " + ticket.getAmount());
        });
        try {
            ObjectMapper mapper = new ObjectMapper();
            String jsonInString = mapper.writerWithDefaultPrettyPrinter().writeValueAsString(dto);
            System.out.println(jsonInString);
            return jsonInString;
        } catch (JsonGenerationException e) {
            e.printStackTrace();
        } catch (JsonMappingException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

        return "success";
    }

    @PostMapping("/minCategory")
    public String minCategory(@RequestBody List<TicketDto> dto) {
        try{
            ObjectMapper mapper = new ObjectMapper();
            List<Ticket> waybills = TicketMapper.TicketDtoToTicket(dto);
            ITicketStrategy its = new CategoryStrategy(waybills);
            Message msg = its.calculate();
            if (!msg.isSuccess()) {
                msg.setData(mapper.writerWithDefaultPrettyPrinter().writeValueAsString(dto));
            }
            return mapper.writerWithDefaultPrettyPrinter().writeValueAsString(msg);
        } catch (JsonGenerationException e) {
            e.printStackTrace();
        } catch (JsonMappingException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return "success";
    }

    @PostMapping("/min")
    public String minWeight(@RequestBody List<TicketDto> dto) {
        List<Ticket> waybills = TicketMapper.TicketDtoToTicket(dto);
        ITicketStrategy its = new MinimumWeightStrategy(waybills);
        Message msg = its.calculate();
        System.out.println(msg.getData());
        return msg.getData();
    }
}
