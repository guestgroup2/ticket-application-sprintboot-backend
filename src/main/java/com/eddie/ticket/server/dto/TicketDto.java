package com.eddie.ticket.server.dto;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class TicketDto {
    private int id;
    private int ticketId;
    private String ticketNo;
    private String passenger;
    private String original;
    private String destination;
    private int itemId;
    private String itemNo;
    private double weight;
    private String category;
    private String clerk;
    private double charge;
    private double amount;

    public TicketDto() {}
    public TicketDto(TicketDtoBuilder builder) {
        this.id = builder.id;
        this.ticketId = builder.ticketId;
        this.ticketNo = builder.ticketNo;
        this.passenger = builder.passenger;
        this.original = builder.original;
        this.destination = builder.destination;
        this.itemNo = builder.itemNo;
        this.itemId = builder.itemId;
        this.weight = builder.weight;
        this.category = builder.category;
        this.clerk = builder.clerk;
        this.charge = builder.charge;
        this.amount = builder.amount;
    }

    public static class TicketDtoBuilder {
        protected int id;
        protected int ticketId;
        protected String ticketNo;
        protected String passenger;
        protected String original;
        protected String destination;
        protected int itemId;
        protected String itemNo;
        protected double weight;
        protected String category;
        protected String clerk;
        protected double charge;
        protected double amount;

        public TicketDtoBuilder id (int id) {
            this.id = id;
            return this;
        }
        public TicketDtoBuilder ticketId (int ticketId) {
            this.ticketId = ticketId;
            return this;
        }
        public TicketDtoBuilder ticketNo (String ticketNo) {
            this.ticketNo = ticketNo;
            return this;
        }
        public TicketDtoBuilder passenger (String passenger) {
            this.passenger = passenger;
            return this;
        }
        public TicketDtoBuilder original (String original) {
            this.original = original;
            return this;
        }
        public TicketDtoBuilder destination (String destination) {
            this.destination = destination;
            return this;
        }

        public TicketDtoBuilder itemId (int itemId) {
            this.itemId = itemId;
            return this;
        }
        public TicketDtoBuilder itemNo (String itemNo) {
            this.itemNo = itemNo;
            return this;
        }
        public TicketDtoBuilder weight (double weight) {
            this.weight = weight;
            return this;
        }
        public TicketDtoBuilder category (String category) {
            this.category = category;
            return this;
        }
        public TicketDtoBuilder charge (double charge) {
            this.charge = charge;
            return this;
        }
        public TicketDtoBuilder clerk (String clerk) {
            this.clerk = clerk;
            return this;
        }
        public TicketDtoBuilder amount (double amount) {
            this.amount = amount;
            return this;
        }
        public TicketDto build () {
            return new TicketDto(this);
        }
    }
}
