package com.eddie.ticket.server.entity;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class Item implements Cloneable {
    private int id;
    private String code;
    private String name;
    private double weight = 0.0;
    private String cat;
    private double charge = 0.0;

    public Item(ItemBuilder builder) {
        this.id = builder.id;
        this.code = builder.code;
        this.name = builder.name;
        this.weight = builder.weight;
        this.cat = builder.cat;
        this.charge = builder.charge;
    }

    public Item() {

    }

    @Override
    protected Object clone() throws CloneNotSupportedException {
        Item item = (Item) super.clone();
        return item;
    }

    public static class ItemBuilder {
        protected int id;
        protected String code;
        protected String name;
        protected double weight = 0.0;
        protected String cat;
        protected double charge = 0.0;

        public ItemBuilder id (int id) {
            this.id = id;
            return this;
        }

        public ItemBuilder code (String code) {
            this.code = code;
            return this;
        }

        public ItemBuilder name (String name) {
            this.name = name;
            return this;
        }

        public ItemBuilder weight (double weight) {
            this.weight = weight;
            return this;
        }

        public ItemBuilder cat (String cat) {
            this.cat = cat;
            return this;
        }

        public ItemBuilder charge (double charge) {
            this.charge = charge;
            return this;
        }

        public Item build () {
            return new Item(this);
        }
    }
}
