package com.eddie.ticket.server.entity;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class Message {
    private String message;
    private boolean success;
    private String data;
}
