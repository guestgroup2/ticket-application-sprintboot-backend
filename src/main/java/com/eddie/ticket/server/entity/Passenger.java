package com.eddie.ticket.server.entity;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class Passenger implements Cloneable {
    private int id;
    private String name;

    @Override
    protected Object clone() throws CloneNotSupportedException {
        Passenger passenger = (Passenger) super.clone();
        return passenger;
    }

}
