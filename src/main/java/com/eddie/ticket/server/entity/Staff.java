package com.eddie.ticket.server.entity;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class Staff implements Cloneable {
    private int id;
    private String name;

    @Override
    protected Object clone() throws CloneNotSupportedException {
        Staff handler = (Staff) super.clone();
        return handler;
    }
}
