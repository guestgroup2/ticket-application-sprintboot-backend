package com.eddie.ticket.server.entity;

import lombok.Getter;
import lombok.Setter;
import org.apache.commons.lang3.RandomStringUtils;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

@Getter
@Setter
public class Ticket implements Cloneable {
    private int id;
    private String ticketNo;
    private Passenger passenger;
    private String original;
    private String destination;
    private String airline;
    private List<Item> luggageList = new ArrayList<Item>();
    private Staff handler;
    private double amount = 0.0;

    public Ticket(TicketBuilder builder) {
        this.id = builder.id;
        this.ticketNo = builder.ticketNo;
        this.passenger = builder.passenger;
        this.original = builder.original;
        this.destination = builder.destination;
        this.airline = builder.airline;
        this.luggageList = builder.luggageList;
        this.amount = builder.amount;
        this.handler = builder.handler;
    }

    public Ticket() {

    }

    @Override
    protected Object clone() throws CloneNotSupportedException {
        Ticket ticket = (Ticket) super.clone();
        ticket.setPassenger((Passenger) ticket.getPassenger().clone());
        ticket.setHandler((Staff) ticket.getHandler());
        ticket.setLuggageList(new ArrayList<Item>());
        return ticket;
    }

    public static class TicketBuilder {
        protected int id;
        protected String ticketNo;
        protected Passenger passenger;
        protected String original;
        protected String destination;
        protected String airline;
        protected List<Item> luggageList = new ArrayList<Item>();
        protected Staff handler;
        protected final int codeLength = 10;
        protected final int passengerNameLength = 5;
        protected double amount = 0.0;

        public TicketBuilder id (int id) {
            this.id = id;
            return this;
        }

        public TicketBuilder ticketNo (String ticketNo) {
            if (ticketNo == null) {
                ticketNo = RandomStringUtils.random(codeLength, true, true);
            }
            this.ticketNo = ticketNo;
            return this;
        }

        public TicketBuilder passenger (Passenger passenger) {
            if (passenger == null) {
                Random z = new Random();
                passenger = new Passenger();
                passenger.setName(RandomStringUtils.random(passengerNameLength, true, false));
            }
            this.passenger = passenger;
            return this;
        }

        public TicketBuilder original (String original) {
            if (original == null) {
                original = "LHR";
            }
            this.original = original;
            return this;
        }

        public TicketBuilder destination (String destination) {
            if (destination == null) {
                destination = "SYD";
            }
            this.destination = destination;
            return this;
        }

        public TicketBuilder airline (String airline) {
            this.airline = airline;
            return this;
        }

        public TicketBuilder luggageList (List<Item> luggageList) {
            if (luggageList == null) {
                luggageList = new ArrayList<Item>();
            }
            if (luggageList.isEmpty() || luggageList.size() == 0) {
                Random itemRandom = null;
                for (int i = 0;i < 5; i++) {
                    itemRandom = new Random();
                    Item item = new Item.ItemBuilder().id(i)
                            .weight(itemRandom.nextInt(35))
                            .code(RandomStringUtils.random(codeLength, true, true))
                            .build();
                    luggageList.add(item);
                }
            }

            this.luggageList = luggageList;
            return this;
        }

        public TicketBuilder handler (Staff handler) {
            if (handler == null) {
                handler = new Staff();
            }
            this.handler = handler;
            return this;
        }

        public TicketBuilder amount (double amount) {
            this.amount = amount;
            return this;
        }

        public Ticket build() {
            return new Ticket(this);
        }
    }
}
