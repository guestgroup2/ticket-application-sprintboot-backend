package com.eddie.ticket.server.entity;

import lombok.Getter;
import lombok.Setter;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

@Getter
@Setter
public abstract class TicketStrategy {
    private Ticket ticket;
    private List<Ticket> waybills;
    private List<Item> freeHeavy;
    private List<Item> freeLite;
    private List<Item> chargeHeavy;
    private List<Item> betweenHeavy;

    public TicketStrategy() {}
    public TicketStrategy(List<Ticket> waybills) {
        this.waybills = waybills;
    }

    public abstract double[] calculateMinimumWeight();

    public void calc() {
        double[] total = calculateMinimumWeight();
        List<Item> results = new ArrayList<Item>();
        results.addAll(this.getFreeLite());
        results.addAll(this.getBetweenHeavy());
        results.addAll(this.getChargeHeavy());
        this.ticket.getLuggageList().clear();
        this.ticket.getLuggageList().addAll(results);
        double totalFinal = total[0] + total[1] + total[2];
        ticket.setAmount(totalFinal);
    }

    public void initTicket(Ticket ticket) {
        this.ticket = ticket;
        this.freeHeavy = this.ticket.getLuggageList().stream().filter( (item) -> item.getWeight() <= 25.0).collect(Collectors.toList());
        this.freeLite = this.freeHeavy.stream().filter( (item) -> item.getWeight() <= 7.0).collect(Collectors.toList());
        this.freeLite.sort(Comparator.comparingDouble(Item::getWeight));
        this.chargeHeavy = this.ticket.getLuggageList().stream().filter( (item) -> item.getWeight() > 25.0).collect(Collectors.toList());
        this.chargeHeavy.sort(Comparator.comparingDouble(Item::getWeight));
        this.betweenHeavy = this.freeHeavy.stream().filter( (item) -> item.getWeight() > 7).collect(Collectors.toList());
    }
}