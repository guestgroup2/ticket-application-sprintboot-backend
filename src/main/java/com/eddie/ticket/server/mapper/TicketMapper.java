package com.eddie.ticket.server.mapper;

import com.eddie.ticket.server.dto.TicketDto;
import com.eddie.ticket.server.entity.Item;
import com.eddie.ticket.server.entity.Passenger;
import com.eddie.ticket.server.entity.Staff;
import com.eddie.ticket.server.entity.Ticket;

import java.util.ArrayList;
import java.util.List;

public class TicketMapper {
    public static List<TicketDto> TicketToTicketDto (List<Ticket> ticketList) {
        List<TicketDto> dto = new ArrayList<TicketDto>();
        int counter = 0;
        for (int j = 0; j < ticketList.size();j++) {
            for (int k = 0; k < ticketList.get(j).getLuggageList().size();k++) {
                counter = counter +1;
                dto.add(new TicketDto.TicketDtoBuilder().id(counter).ticketId(ticketList.get(j).getId())
                        .ticketNo(ticketList.get(j).getTicketNo())
                        .passenger(ticketList.get(j).getPassenger().getName())
                        .original(ticketList.get(j).getOriginal())
                        .destination(ticketList.get(j).getDestination())
                        .itemId(ticketList.get(j).getLuggageList().get(k).getId())
                        .itemNo(ticketList.get(j).getLuggageList().get(k).getCode())
                        .category(ticketList.get(j).getLuggageList().get(k).getCat())
                        .clerk(ticketList.get(j).getHandler().getName())
                        .charge(ticketList.get(j).getLuggageList().get(k).getCharge())
                        .amount(ticketList.get(j).getAmount())
                        .weight(ticketList.get(j).getLuggageList().get(k).getWeight())
                        .build());
            }
        }
        return dto;
    }

    public static List<Ticket> TicketDtoToTicket (List<TicketDto> ticketDtoList) {
        List<Ticket> tickets = new ArrayList<Ticket>();
        int counter = 0;
        Ticket ticket = null;
        for (int j = 0; j < ticketDtoList.size();j++) {
            Item item = new Item.ItemBuilder().id(ticketDtoList.get(j).getItemId())
                    .weight(ticketDtoList.get(j).getWeight())
                    .code(ticketDtoList.get(j).getItemNo())
                    .cat(ticketDtoList.get(j).getCategory())
                    .charge(ticketDtoList.get(j).getCharge())
                    .build();
            if (j > 0 && ticketDtoList.get(j).getTicketId() != ticketDtoList.get(j-1).getTicketId()) {
                tickets.add(ticket);
            }
            if (j == 0 || ticketDtoList.get(j).getTicketId() != ticketDtoList.get(j-1).getTicketId()) {
                Staff handler = new Staff();
                handler.setName(ticketDtoList.get(j).getClerk());
                Passenger passenger = new Passenger();
                passenger.setName(ticketDtoList.get(j).getPassenger());
                ticket = new Ticket.TicketBuilder().id(ticketDtoList.get(j).getTicketId())
                        .handler(handler)
                        .original(ticketDtoList.get(j).getOriginal())
                        .destination(ticketDtoList.get(j).getDestination())
                        .passenger(passenger)
                        .ticketNo(ticketDtoList.get(j).getTicketNo())
                        .amount(ticketDtoList.get(j).getAmount())
                        .build();
                ticket.setLuggageList(new ArrayList<Item>());
            }
            ticket.getLuggageList().add(item);
            if (j == ticketDtoList.size() - 1) {
                tickets.add(ticket);
            }
        }
        return tickets;
    }
}
