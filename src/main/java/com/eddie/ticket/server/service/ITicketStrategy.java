package com.eddie.ticket.server.service;

import com.eddie.ticket.server.entity.Item;
import com.eddie.ticket.server.entity.Message;
import com.fasterxml.jackson.databind.node.ObjectNode;

import java.util.List;

public interface ITicketStrategy {
    Message calculate();
    ObjectNode validate(List<Item> items);
}
