package com.eddie.ticket.server.service;

import com.eddie.ticket.server.entity.Message;
import com.eddie.ticket.server.entity.Ticket;
import com.eddie.ticket.server.dto.TicketDto;

import java.util.List;

public interface TicketService {
    public List<Ticket> getTickets();
}
