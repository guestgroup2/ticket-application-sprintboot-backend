package com.eddie.ticket.server.service.impl;

import com.eddie.ticket.server.dto.TicketDto;
import com.eddie.ticket.server.entity.Item;
import com.eddie.ticket.server.entity.Message;
import com.eddie.ticket.server.entity.Ticket;
import com.eddie.ticket.server.entity.TicketStrategy;
import com.eddie.ticket.server.mapper.TicketMapper;
import com.eddie.ticket.server.service.ITicketStrategy;
import com.fasterxml.jackson.core.JsonGenerationException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;

import java.io.IOException;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

public class CategoryStrategy extends TicketStrategy implements ITicketStrategy {

    public CategoryStrategy() {}
    public CategoryStrategy(List<Ticket> waybills) {
        super(waybills);
    }

    public Message getCategoryPayment() {
        System.out.println("start");
        ObjectNode res = validate(this.getTicket().getLuggageList());
        Message msg = new Message();
        if (!res.get("success").booleanValue()) {
            msg.setSuccess(false);
            msg.setMessage(res.get("message").textValue());
            return msg;
        }
        this.calc();
        msg.setSuccess(true);
        msg.setMessage("");
        return msg;
    }

    @Override
    public double[] calculateMinimumWeight() {
        double[] total = new double[] {0,0,0};
        for (int j = 0;j < this.getFreeLite().size();j++) {
            if (!"FREE".equalsIgnoreCase(this.getFreeLite().get(j).getCat())) {
                total[1] = total[1] + 10;
                this.getFreeLite().get(j).setCharge(10);
            } else {
                this.getFreeLite().get(j).setCharge(0);
            }
        }

        for (int i = 0;i < this.getChargeHeavy().size();i++) {
            if (this.getBetweenHeavy() != null && this.getBetweenHeavy().size() == 0 && "FREE_OVER".equalsIgnoreCase(this.getChargeHeavy().get(i).getCat())) {
                this.getChargeHeavy().get(i).setCharge(((this.getChargeHeavy().get(i).getWeight() - 25) * 5));
            } else {
                this.getChargeHeavy().get(i).setCharge(25 + ((this.getChargeHeavy().get(i).getWeight() - 25) * 5));
            }
            total[0] = total[0] + this.getChargeHeavy().get(i).getCharge();
        }

        for (int k = 0;k < this.getBetweenHeavy().size();k++) {
            if ("FREE_OVER".equalsIgnoreCase(this.getBetweenHeavy().get(k).getCat()) && this.getFreeLite() != null && this.getFreeLite().size() == 0) {
                this.getBetweenHeavy().get(k).setCharge(((this.getBetweenHeavy().get(k).getWeight() - 7) * 5));
            } else if (!"FREE".equalsIgnoreCase(this.getBetweenHeavy().get(k).getCat()) && "HEAVY".equalsIgnoreCase(this.getBetweenHeavy().get(k).getCat())) {
                this.getBetweenHeavy().get(k).setCharge(25);
            } else if ("FREE".equalsIgnoreCase(this.getBetweenHeavy().get(k).getCat())) {
                this.getBetweenHeavy().get(k).setCharge(0);
            }
            total[2] = total[2] + this.getBetweenHeavy().get(k).getCharge();
        }
        return total;
    }

    @Override
    public Message calculate() {
        Message msg = new Message();
        try {
            ObjectMapper mapper = new ObjectMapper();

            for (int p = 0; p < this.getWaybills().size(); p++) {
                this.initTicket(this.getWaybills().get(p));
                this.getBetweenHeavy().sort(Comparator.comparingDouble(Item::getWeight).reversed());
                msg = this.getCategoryPayment();
                if (!msg.isSuccess()) {
                    return msg;
                }
            }
            List<TicketDto> alteredDto = TicketMapper.TicketToTicketDto(this.getWaybills());

            String jsonInString = mapper.writerWithDefaultPrettyPrinter().writeValueAsString(alteredDto);
            msg.setData(jsonInString);
        } catch (JsonGenerationException e) {
            e.printStackTrace();
        } catch (JsonMappingException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return msg;
    }

    @Override
    public ObjectNode validate(List<Item> items) {
        List<Item> lessTwoFive = items.stream().filter( (item) -> item.getWeight() <= 25.0).collect(Collectors.toList());
        List<Item> freeLite = lessTwoFive.stream().filter( (item) -> item.getWeight() <= 7.0).collect(Collectors.toList());
        List<Item> largerTwoFive = items.stream().filter( (item) -> item.getWeight() > 25.0).collect(Collectors.toList());
        List<Item> lessTwoFiveLargerThanSeven = lessTwoFive.stream().filter( (item) -> item.getWeight() > 7.0).collect(Collectors.toList());

        //item.getWeight() > 7.0 && <= 25
        List<Item> twoFiveWithFreeOver = lessTwoFiveLargerThanSeven.stream().filter( (item) -> "FREE_OVER".equalsIgnoreCase(item.getCat())).collect(Collectors.toList());
        List<Item> twoFiveWithFree = lessTwoFiveLargerThanSeven.stream().filter( (item) -> "FREE".equalsIgnoreCase(item.getCat())).collect(Collectors.toList());
        List<Item> twoFiveWithLight = lessTwoFiveLargerThanSeven.stream().filter( (item) -> "LIGHT".equalsIgnoreCase(item.getCat())).collect(Collectors.toList());
        List<Item> twoFiveWithExtreme = lessTwoFiveLargerThanSeven.stream().filter( (item) -> "EXT".equalsIgnoreCase(item.getCat())).collect(Collectors.toList());


        //item.getWeight() <= 7.0
        List<Item> sevenWithFree = freeLite.stream().filter( (item) -> "FREE".equalsIgnoreCase(item.getCat())).collect(Collectors.toList());
        List<Item> sevenWithExtreme = freeLite.stream().filter( (item) -> "EXT".equalsIgnoreCase(item.getCat())).collect(Collectors.toList());
        List<Item> sevenWithHeavy = freeLite.stream().filter( (item) -> "HEAVY".equalsIgnoreCase(item.getCat())).collect(Collectors.toList());
        List<Item> sevenWithFreeOver = freeLite.stream().filter( (item) -> "FREE_OVER".equalsIgnoreCase(item.getCat())).collect(Collectors.toList());

        //item.getWeight() > 25.0
        List<Item> largeTwoFiveWithFreeOver = largerTwoFive.stream().filter( (item) -> "FREE_OVER".equalsIgnoreCase(item.getCat())).collect(Collectors.toList());
        List<Item> largeTwoFiveWithFree = largerTwoFive.stream().filter( (item) -> "FREE".equalsIgnoreCase(item.getCat())).collect(Collectors.toList());
        List<Item> largeTwoFiveWithLight = largerTwoFive.stream().filter( (item) -> "LIGHT".equalsIgnoreCase(item.getCat())).collect(Collectors.toList());
        List<Item> largeTwoFiveWithHeavy = largerTwoFive.stream().filter( (item) -> item.getWeight() > 25.0 && "HEAVY".equalsIgnoreCase(item.getCat())).collect(Collectors.toList());

        ObjectMapper mapper = new ObjectMapper();
        ObjectNode rootNode = mapper.createObjectNode();
        if (sevenWithFree.size() == 0 && twoFiveWithFreeOver.size() == 0 && twoFiveWithFree.size() == 0 && largeTwoFiveWithFreeOver.size() == 0) {
            System.out.println("At least one item is free or free + overweight");
            rootNode.put("message", "At least one item is free or free + overweight");
            rootNode.put("success", false);
            return rootNode;
        } else if (sevenWithFree.size() > 1) {
            System.out.println("Only one free item <= 7");
            rootNode.put("message", "Only one free item <= 7");
            rootNode.put("success", false);
            return rootNode;
        } else if (twoFiveWithFree.size() > 1) {
            System.out.println("Only one free item > 7 and <= 25");
            rootNode.put("message", "Only one free item > 7 and <= 25");
            rootNode.put("success", false);
            return rootNode;
        } else if (largeTwoFiveWithFreeOver.size() > 1) {
            System.out.println("Only one free + overweight item > 25");
            rootNode.put("message", "Only one free + overweight item > 25");
            rootNode.put("success", false);
            return rootNode;
        } else if (twoFiveWithFreeOver.size() > 1) {
            System.out.println("Only one free + overweight item > 7 and <= 25");
            rootNode.put("message", "Only one free + overweight item > 7 and <= 25");
            rootNode.put("success", false);
            return rootNode;
        } else if (sevenWithFree.size() > 0 && twoFiveWithFreeOver.size() > 0) {
            System.out.println("Cannot set item free <= 7 + free + overweight > 7 and <=25");
            rootNode.put("message", "Cannot set item free <= 7 + free + overweight > 7 and <=25");
            rootNode.put("success", false);
            return rootNode;
        } else if (twoFiveWithFree.size() > 0 && largeTwoFiveWithFreeOver.size() > 0) {
            System.out.println("Cannot set item free > 7 and <=25 + free + overweight > 25");
            rootNode.put("message", "Cannot set item free > 7 and <=25 + free + overweight > 25");
            rootNode.put("success", false);
            return rootNode;
        } else if (sevenWithFreeOver.size() > 0) {
            System.out.println("Impossible to set free + overweight <= 7");
            rootNode.put("message", "Impossible to set free + overweight <= 7");
            rootNode.put("success", false);
            return rootNode;
        } else if (largeTwoFiveWithFree.size() > 0) {
            System.out.println("Impossible to set free > 25");
            rootNode.put("message", "Impossible to set free > 25");
            rootNode.put("success", false);
            return rootNode;
        } else if (largeTwoFiveWithLight.size() > 0) {
            System.out.println("Impossible to set Light > 25");
            rootNode.put("message", "Impossible to set Light > 25");
            rootNode.put("success", false);
            return rootNode;
        } else if (largeTwoFiveWithHeavy.size() > 0) {
            System.out.println("Impossible to set Heavy > 25");
            rootNode.put("message", "Impossible to set Heavy > 25");
            rootNode.put("success", false);
            return rootNode;
        } else if (sevenWithExtreme.size() > 0) {
            System.out.println("Impossible to set Extreme Large <= 7");
            rootNode.put("message", "Impossible to set Extreme Large <= 7");
            rootNode.put("success", false);
            return rootNode;
        } else if (sevenWithHeavy.size() > 0) {
            System.out.println("Impossible to set Heavy <= 7");
            rootNode.put("message", "Impossible to set Heavy <= 7");
            rootNode.put("success", false);
            return rootNode;
        } else if (twoFiveWithExtreme.size() > 0) {
            System.out.println("Impossible to set Extreme Large > 7 and <= 25");
            rootNode.put("message", "Impossible to set Extreme Large > 7 and <= 25");
            rootNode.put("success", false);
            return rootNode;
        } else if (twoFiveWithLight.size() > 0) {
            System.out.println("Impossible to set Light > 7 and <= 25");
            rootNode.put("message", "Impossible to set Light > 7 and <= 25");
            rootNode.put("success", false);
            return rootNode;
        } else if (sevenWithFree.size() == 0 && freeLite.size() > 0) {
            System.out.println("At least one item is set free < 7 when there is item < 7");
            rootNode.put("message", "At least one item is set free < 7 when there is item < 7");
            rootNode.put("success", false);
            return rootNode;
        } else if (twoFiveWithFree.size() == 0 && lessTwoFiveLargerThanSeven.size() > 0) {
            System.out.println("At least one item is set free when there is item between 7 and 25");
            rootNode.put("message", "At least one item is set free when there is item between 7 and 25");
            rootNode.put("success", false);
            return rootNode;
        } else if (lessTwoFiveLargerThanSeven.size() > 0 && largeTwoFiveWithFreeOver.size() > 0) {
            System.out.println("If item between 7 and 25 list is not empty, cannot set free + overweight on > 25");
            rootNode.put("message", "If item between 7 and 25 list is not empty, cannot set free + overweight on > 25");
            rootNode.put("success", false);
            return rootNode;
        }
        rootNode.put("message", "");
        rootNode.put("success", true);
        return rootNode;
    }
}
