package com.eddie.ticket.server.service.impl;

import com.eddie.ticket.server.dto.TicketDto;
import com.eddie.ticket.server.entity.Item;
import com.eddie.ticket.server.entity.Message;
import com.eddie.ticket.server.entity.Ticket;
import com.eddie.ticket.server.entity.TicketStrategy;
import com.eddie.ticket.server.mapper.TicketMapper;
import com.eddie.ticket.server.service.ITicketStrategy;
import com.fasterxml.jackson.core.JsonGenerationException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;

import java.io.IOException;
import java.util.Comparator;
import java.util.List;

public class MinimumWeightStrategy extends TicketStrategy implements ITicketStrategy {
    public MinimumWeightStrategy() {}
    public MinimumWeightStrategy(List<Ticket> waybills) {
        super(waybills);
    }

    public Message getMiniPayment() {
        System.out.println("start");
        Message msg = new Message();
        this.calc();
        msg.setSuccess(true);
        msg.setMessage("");
        return msg;
    }

    @Override
    public double[] calculateMinimumWeight() {
        double[] total = new double[] {0,0,0};
        for (int j = 0;j < this.getFreeLite().size();j++) {
            if (j > 0) {
                total[1] = total[1] + 10;
                this.getFreeLite().get(j).setCharge(10);
            } else {
                this.getFreeLite().get(j).setCharge(0);
            }
        }

        for (int i = 0;i < this.getChargeHeavy().size();i++) {
            if (this.getBetweenHeavy() != null && this.getBetweenHeavy().size() == 0 && i == 0) {
                this.getChargeHeavy().get(i).setCharge(((this.getChargeHeavy().get(i).getWeight() - 25) * 5));
            } else {
                this.getChargeHeavy().get(i).setCharge(25 + ((this.getChargeHeavy().get(i).getWeight() - 25) * 5));
            }
            total[0] = total[0] + this.getChargeHeavy().get(i).getCharge();
        }

        for (int k = 0;k < this.getBetweenHeavy().size();k++) {
            if (k == 1 && this.getFreeLite() != null && this.getFreeLite().size() == 0) {
                this.getBetweenHeavy().get(k).setCharge(((this.getBetweenHeavy().get(k).getWeight() - 7) * 5));
            } else if (k > 0) {
                this.getBetweenHeavy().get(k).setCharge(25);
            } else {
                this.getBetweenHeavy().get(k).setCharge(0);
            }
            total[2] = total[2] + this.getBetweenHeavy().get(k).getCharge();
        }
        return total;
    }


    @Override
    public Message calculate() {
        Message msg = new Message();
        this.getWaybills().forEach(ticket -> {
            this.initTicket(ticket);
            this.getBetweenHeavy().sort(Comparator.comparingDouble(Item::getWeight));
            this.getMiniPayment();
            ticket.getLuggageList().forEach( item -> {
                System.out.println(ticket.getTicketNo() + " - " + item.getCode() + ": " + item.getWeight());
            });
            System.out.println(ticket.getTicketNo() + " total: " + ticket.getAmount());
        });
        List<TicketDto> alteredDto = TicketMapper.TicketToTicketDto(this.getWaybills());
        try {
            ObjectMapper mapper = new ObjectMapper();
            String jsonInString = mapper.writerWithDefaultPrettyPrinter().writeValueAsString(alteredDto);
            System.out.println(jsonInString);
            msg.setData(jsonInString);
            return msg;
        } catch (JsonGenerationException e) {
            e.printStackTrace();
        } catch (JsonMappingException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return msg;
    }

    @Override
    public ObjectNode validate(List<Item> items) {
        System.out.println("waiting for implementation");
        return null;
    }
}
