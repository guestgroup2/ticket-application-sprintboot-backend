package com.eddie.ticket.server.service.impl;

import com.eddie.ticket.server.entity.*;
import com.eddie.ticket.server.service.TicketService;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class TicketServiceImpl implements TicketService {

    @Override
    public List<Ticket> getTickets() {
        Staff clerk = new Staff();
        clerk.setName("Mary");
        List<Ticket> ticketList = new ArrayList<Ticket>();
        for (int i = 0; i < 1; i++) {
            ticketList.add(new Ticket.TicketBuilder().id(i)
                    .handler(clerk)
                    .original(null)
                    .destination(null)
                    .passenger(null)
                    .ticketNo(null)
                    .luggageList(null).build());
        }

        return ticketList;
    }
}
