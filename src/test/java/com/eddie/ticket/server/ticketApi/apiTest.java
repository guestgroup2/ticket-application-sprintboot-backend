package com.eddie.ticket.server.ticketApi;

import com.eddie.ticket.server.api.ticketApi;
import com.eddie.ticket.server.entity.Message;
import com.eddie.ticket.server.dto.TicketDto;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;

public class apiTest {

    @Test
    public void generateTicketTest() throws Exception{
        ticketApi t = new ticketApi();
        String json = t.generateTickets();
    }

    @Test
    public void calculateMinPrice() throws Exception{
        ticketApi t = new ticketApi();
        List<TicketDto> dto = new ArrayList<TicketDto>();
        dto.add(new TicketDto.TicketDtoBuilder().id(0).ticketId(0)
                .ticketNo("YwdnARoq7t")
                .passenger("dblwb")
                .original("LHR")
                .destination("SYD")
                .itemId(0)
                .itemNo("zPnNUSn0Rd")
                .category("")
                .clerk("Mary")
                .charge(0)
                .amount(0)
                .weight(14)
                .build());
        dto.add(new TicketDto.TicketDtoBuilder().id(0).ticketId(0)
                .ticketNo("YwdnARoq7t")
                .passenger("dblwb")
                .original("LHR")
                .destination("SYD")
                .itemId(1)
                .itemNo("cMsmP1V0KM")
                .category("")
                .clerk("Mary")
                .charge(0)
                .amount(0)
                .weight(10)
                .build());
        dto.add(new TicketDto.TicketDtoBuilder().id(0).ticketId(0)
                .ticketNo("YwdnARoq7t")
                .passenger("dblwb")
                .original("LHR")
                .destination("SYD")
                .itemId(2)
                .itemNo("coNCpZmHn9")
                .category("")
                .clerk("Mary")
                .charge(0)
                .amount(0)
                .weight(33)
                .build());
        dto.add(new TicketDto.TicketDtoBuilder().id(0).ticketId(0)
                .ticketNo("YwdnARoq7t")
                .passenger("dblwb")
                .original("LHR")
                .destination("SYD")
                .itemId(3)
                .itemNo("U0eo1LMRkV")
                .category("")
                .clerk("Mary")
                .charge(0)
                .amount(0)
                .weight(5)
                .build());
        dto.add(new TicketDto.TicketDtoBuilder().id(0).ticketId(0)
                .ticketNo("YwdnARoq7t")
                .passenger("dblwb")
                .original("LHR")
                .destination("SYD")
                .itemId(4)
                .itemNo("z3curVqRf6")
                .category("")
                .clerk("Mary")
                .charge(0)
                .amount(0)
                .weight(29)
                .build());
        List<TicketDto> alteredDto = t.minWeight(dto);
        String[] result = new String[]{"1", "1", "1", "1", "1"};
        for (int i = 0;i < alteredDto.size(); i++) {
            System.out.println("id:" + alteredDto.get(i).getItemId() + " weight " + alteredDto.get(i).getWeight() + " Charge: "+ alteredDto.get(i).getCharge());
            if(alteredDto.get(i).getItemId() == 3 && alteredDto.get(i).getWeight() == 5 && alteredDto.get(i).getCharge() == 0) {
                result[0] = "0";
            } else if(alteredDto.get(i).getItemId() == 1 && alteredDto.get(i).getWeight() == 10.0 && alteredDto.get(i).getCharge() == 0.0) {
                result[1] = "0";
            } else if(alteredDto.get(i).getItemId() == 0 && alteredDto.get(i).getWeight() == 14.0 && alteredDto.get(i).getCharge() == 25.0) {
                result[2] = "0";
            } else if(alteredDto.get(i).getItemId() == 4 && alteredDto.get(i).getWeight() == 29.0 && alteredDto.get(i).getCharge() == 45.0) {
                result[3] = "0";
            } else if(alteredDto.get(i).getItemId() == 2 && alteredDto.get(i).getWeight() == 33.0 && alteredDto.get(i).getCharge() == 65.0) {
                result[4] = "0";
            }
        }
        for (int j = 0;j < result.length; j++) {
            if (result[j] == "1") {
                throw new Exception();
            }
        }
    }

    @Test
    public void calculatePriceByCategory() throws Exception {
        ObjectMapper mapper = new ObjectMapper();
        ticketApi t = new ticketApi();
        List<TicketDto> dto = new ArrayList<TicketDto>();
        dto.add(new TicketDto.TicketDtoBuilder().id(0).ticketId(0)
                .ticketNo("YwdnARoq7t")
                .passenger("dblwb")
                .original("LHR")
                .destination("SYD")
                .itemId(0)
                .itemNo("zPnNUSn0Rd")
                .category("FREE")
                .clerk("Mary")
                .charge(0)
                .amount(0)
                .weight(14)
                .build());
        dto.add(new TicketDto.TicketDtoBuilder().id(0).ticketId(0)
                .ticketNo("YwdnARoq7t")
                .passenger("dblwb")
                .original("LHR")
                .destination("SYD")
                .itemId(1)
                .itemNo("cMsmP1V0KM")
                .category("HEAVY")
                .clerk("Mary")
                .charge(0)
                .amount(0)
                .weight(10)
                .build());
        dto.add(new TicketDto.TicketDtoBuilder().id(0).ticketId(0)
                .ticketNo("YwdnARoq7t")
                .passenger("dblwb")
                .original("LHR")
                .destination("SYD")
                .itemId(2)
                .itemNo("coNCpZmHn9")
                .category("EXT")
                .clerk("Mary")
                .charge(0)
                .amount(0)
                .weight(33)
                .build());
        dto.add(new TicketDto.TicketDtoBuilder().id(0).ticketId(0)
                .ticketNo("YwdnARoq7t")
                .passenger("dblwb")
                .original("LHR")
                .destination("SYD")
                .itemId(3)
                .itemNo("U0eo1LMRkV")
                .category("FREE")
                .clerk("Mary")
                .charge(0)
                .amount(0)
                .weight(5)
                .build());
        dto.add(new TicketDto.TicketDtoBuilder().id(0).ticketId(0)
                .ticketNo("YwdnARoq7t")
                .passenger("dblwb")
                .original("LHR")
                .destination("SYD")
                .itemId(4)
                .itemNo("z3curVqRf6")
                .category("EXT")
                .clerk("Mary")
                .charge(0)
                .amount(0)
                .weight(29)
                .build());
        Message msg = t.minCategory(dto);
        List<TicketDto> alteredDto = mapper.readValue(msg.getData(), new TypeReference<ArrayList<TicketDto>>(){});
        String[] result = new String[]{"1","1", "1", "1", "1", "1"};
        if(msg.isSuccess()) {
            result[5] = "0";
        }
        for (int i = 0;i < alteredDto.size(); i++) {
            System.out.println("id:" + alteredDto.get(i).getItemId() + " weight " + alteredDto.get(i).getWeight() + " Charge: "+ alteredDto.get(i).getCharge());
            if(alteredDto.get(i).getItemId() == 3 && alteredDto.get(i).getWeight() == 5 && alteredDto.get(i).getCharge() == 0) {
                result[0] = "0";
            } else if(alteredDto.get(i).getItemId() == 1 && alteredDto.get(i).getWeight() == 10.0 && alteredDto.get(i).getCharge() == 25.0) {
                result[1] = "0";
            } else if(alteredDto.get(i).getItemId() == 0 && alteredDto.get(i).getWeight() == 14.0 && alteredDto.get(i).getCharge() == 0.0) {
                result[2] = "0";
            } else if(alteredDto.get(i).getItemId() == 4 && alteredDto.get(i).getWeight() == 29.0 && alteredDto.get(i).getCharge() == 45.0) {
                result[3] = "0";
            } else if(alteredDto.get(i).getItemId() == 2 && alteredDto.get(i).getWeight() == 33.0 && alteredDto.get(i).getCharge() == 65.0) {
                result[4] = "0";
            }
        }
        for (int j = 0;j < result.length; j++) {
            if (result[j] == "1") {
                throw new Exception();
            }
        }
    }
}
